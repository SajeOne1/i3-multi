guint getWorkspaceCount(i3ipcConnection* conn){
    i3ipcCon* tree = i3ipc_connection_get_tree (conn, NULL);
    GList* workspaces = i3ipc_con_workspaces(tree);
    guint length = g_list_length(workspaces);

    return length;
}

gchar** getAllWorkspaces(i3ipcConnection* conn){
    i3ipcCon* tree = i3ipc_connection_get_tree (conn, NULL);
    GList* workspaces = i3ipc_con_workspaces(tree);
    guint length = g_list_length(workspaces);

    char** retSpaces = malloc((length + 1) * sizeof(char*));

    int i;
    for(i = 0; i < length; i++){
        i3ipcCon* curSpace = (i3ipcCon*)g_list_nth_data(workspaces, i); 
        gchar* curName = (gchar*)i3ipc_con_get_name(curSpace);

        retSpaces[i] = malloc(sizeof(curName));

        strcpy(retSpaces[i], curName);
    }   
    retSpaces[length] = '\0';
    
    return retSpaces;
}


gchar* getFocusedWorkspaceName(i3ipcConnection* conn){
    if(conn != NULL){
        i3ipcCon* tree = i3ipc_connection_get_tree (conn, NULL);
        i3ipcCon* focused = i3ipc_con_find_focused(tree);
        gchar* focusedName = i3ipc_con_get_name(focused);
        i3ipcCon* workspace = i3ipc_con_workspace (focused);


        char* retName;
        if(workspace != NULL){
            gchar* name = (gchar*) i3ipc_con_get_name (workspace);
            retName = (gchar*) malloc(sizeof(name));
            strcpy(retName, name);
        }else{
            retName = (gchar*) malloc(sizeof(focusedName));
            strcpy(retName, focusedName);
        }   

        return retName;

            
    }   

    return NULL;
}
