//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// Author: Shane "SajeOne" Brown
// Date: 17/05/2016
// Revision: 1
// Description: Common utility functions for use in main application
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

#include <assert.h>
#include <string.h>

char** str_split(char* a_str, const char a_delim);
int stringToInt(char* str);
int indexOf(char needle, const char* haystack);
char* subStr(int endIndex, char* str);
int getArrNumDigits(int arr[], int size);
int transform_ints_to_string(int const* data, int data_length, char* output, int output_length);
int trimArr(int arr[], int needle, int size);
int* to_vector(const char* str);
int charInString(char c, char* s);

int charInString(char c, char* s){
    int i;
    for(i = 0; i < sizeof(s) / sizeof(char); i++){
        if(s[i] == c){
            return 1;
        }
    }

    return 0;
}

int* to_vector(const char* str){
    int colonIndex;
    colonIndex = indexOf(':', str);


    if(colonIndex != -1){
        // get substring of str
        str = subStr(colonIndex, (char*) str);
    }

    char** vecArray;
    vecArray = str_split((char*) str, ',');
    

    // Find length of array using null terminator
    int arrLength = -1; 
    while (vecArray[++arrLength] != '\0') { /* do nothing */}
    
    int vec[arrLength];

    int i;
    for(i = 0; i < arrLength; i++){
        vec[i] = stringToInt(vecArray[i]);
    }
    
    int* returnVec;

    returnVec = malloc(sizeof(vec));

    memcpy(returnVec, vec, sizeof(vec));

    return returnVec;
}

int trimArr(int arr[], int needle, int size){
    while(arr[size - 1] == needle) { if(size <= 0) break; size--; }
    return size;
}

// Credit to Roger Pate
int transform_ints_to_string(int const* data, int data_length, char* output, int output_length){
    // precondition: non-null pointers
    assert(data);
    assert(output);
    // precondition: valid data length
    assert(data_length >= 0);
    // precondition: output has room for null
    assert(output_length >= 1);

    int written = 0;
    for (; data_length; data_length--) {
        int length;
        printf("Data: %d\n", *data);
        if(data_length >= 2)
            length = snprintf(output, output_length, "%d", *data++);
        else if((*data) != 0)
            length = snprintf(output, output_length, ",%d", *data++);



        if (length >= output_length) {
            // not enough space
            return -1;
        }
        written += length;
        output += length;
        output_length -= length;
    }

    return written;
}

int getArrNumDigits(int arr[], int size){
    int logSize = 0;
    int i;
    for(i = 0; i < size; i++){
        if(arr[i] == 0){
           logSize++;
        }else{
           logSize += floor (log10 (abs (arr[i]))) + 1;
        }

        if(arr[i] < 0)
            logSize++;
    }

    return logSize;
}

char* subStr(int endIndex, char* str){
    int size;
    size = sizeof(str) / sizeof(char);

    if(size < endIndex && endIndex > 0){
        char* returnStr;

        size_t newSize;
        newSize = sizeof(endIndex + 1) * sizeof(char);

        returnStr = malloc(newSize);
        memcpy(returnStr, str, newSize);

        return returnStr;
    }

    return -1;
}

int indexOf(char needle, const char* haystack){
    const char* ptr = strchr(haystack, needle);

    if(ptr){
        return ptr - haystack;
    }

    return -1; 
}

int stringToInt(char* str){

    int intVal;
    char *ptr;

    intVal = strtol(str, &ptr, 10);

    if(*ptr == '\n' || *ptr == '\0'){
        return intVal;
    }   

    return 0;
}

// Credit to hmjd <https://stackoverflow.com/users/1033896/hmjd>
char** str_split(char* a_str, const char a_delim)
{
    char** result    = 0;
    size_t count     = 0;
    char* tmp        = a_str;
    char* last_comma = 0;
    char delim[2];
    delim[0] = a_delim;
    delim[1] = 0;

    /* Count how many elements will be extracted. */
    while (*tmp)
    {   
        if (a_delim == *tmp)
        {
            count++;
            last_comma = tmp;
        }
        tmp++;
    }   

    /* Add space for trailing token. */
    count += last_comma < (a_str + strlen(a_str) - 1); 

    /* Add space for terminating null string so caller
       knows where the list of returned strings ends. */
    count++;

    result = malloc(sizeof(char*) * count);

    if (result)
    {
        size_t idx  = 0;

        char* curStr = strdup(a_str);
        char* token = strtok(curStr, delim);

        while (token)
        {
            assert(idx < count);
            *(result + idx++) = strdup(token);
            token = strtok(0, delim);
        }
        assert(idx == count - 1);
        *(result + idx) = 0;
    }

    return result;
}
