#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include <malloc.h>

// External Depends
#include <X11/Xlib.h>
#include <i3ipc-glib/i3ipc-glib.h>

// Self Depends
#include "common_util.c"
#include "i3ipc_util.c" // Utilties that require i3ipc dependency

const int monitors[] = { 1440, 1920, 1680 };

int getLastMonitorSpace(char** workspaces, int length, int is_a){
    int i;
    for(i = length - 1; i > 0; i--){
        int curChar = -1;
        while(workspaces[i][++curChar] != '\0'){
            if(is_a && workspaces[i][curChar] == 'a'){
                return i;
            }else if(!is_a && workspaces[i][curChar] == 'b'){
                return i;
            }
        }
    }

    return -1;
}

// Returns int array of indecies for certain workspace
int* getMonitorSpaces(char** workspaces, int length, int is_a){
    int i;
    int retArray[length];
    for(i = 0; i < length; i++){
        int curChar = -1;
        while(workspaces[i][++curChar] != '\0'){
            if(is_a && workspaces[i][curChar] == 'a'){
                retArray[i] = 1;
                break;
            }else if(!is_a && workspaces[i][curChar] == 'b'){
                retArray[i] = 1;
                break;
            }
            printf("curChar: %c\n",workspaces[i][curChar]);
        }
    }
    return workspaces;
}

int horiz(char* space, int transport){
    // Create i3ipc Connection
    i3ipcConnection *conn;
    conn = i3ipc_connection_new(NULL, NULL);


    // Get Focused Workspace
    gchar* focused = getFocusedWorkspaceName(conn);

    // Used for string concat 
    char* workspaceConst = "workspace \"";
    if(transport){
        workspaceConst = "move container to workspace \"";
    }

    int strSize = sizeof(workspaceConst) + sizeof(space) + 1;

    char cat1[strSize];
    strcpy(cat1, workspaceConst);


    strcat(cat1, space);
    strcat(cat1, "\"");

    i3ipc_connection_command(conn, cat1, NULL);

    // Garbage Collection
    g_object_unref(conn);

    return 0;
}

static int _XlibErrorHandler(Display *display, XErrorEvent *event) {
    fprintf(stderr, "An error occured detecting the mouse position\n");
    return True;
}

int* getPointerPosition(){
    int number_of_screens;
    int i;
    Bool result;
    Window *root_windows;
    Window window_returned;
    int root_x, root_y;
    int win_x, win_y;
    unsigned int mask_return;

    Display *display = XOpenDisplay(NULL);
    assert(display);
    XSetErrorHandler(_XlibErrorHandler);
    number_of_screens = XScreenCount(display);
    root_windows = malloc(sizeof(Window) * number_of_screens);
    for (i = 0; i < number_of_screens; i++) {
        root_windows[i] = XRootWindow(display, i);
    }
    for (i = 0; i < number_of_screens; i++) {
        result = XQueryPointer(display, root_windows[i], &window_returned,
                &window_returned, &root_x, &root_y, &win_x, &win_y,
                &mask_return);
        if (result == True) {
            break;
        }
    }
    if (result != True) {
        fprintf(stderr, "No mouse found.\n");
        return (int*)-1;
    }

    int root[2] = {root_x, root_y};
    int* retRoot = malloc(sizeof(root));
    memcpy(retRoot, root, sizeof(root));

    free(root_windows);
    XCloseDisplay(display);
    return retRoot;

}

char* parseArgs(int argc, char* argv[], int *transport){
    int c;
    char* value;

    while((c = getopt(argc, argv, "m:t:")) != -1){
        switch(c){
            case 'm':
                *transport = 0;
                value = malloc(sizeof(optarg));
                strcpy(value, optarg);
                break;
            case 't':
                *transport = 1;
                value = malloc(sizeof(optarg));
                strcpy(value, optarg);
                break;
            }
    }

    if(!value)
        puts("usage: [-m MOVE_LOC] [-t TRANSPORT_LOC]");


    return value;
}

int main(int argc, char* argv[]) {

    int transport = 0;
    char* value = parseArgs(argc, argv, &transport);
    if(value == 0x0)
        return 1;

    int* pos = getPointerPosition();

    char suffix[3];
    suffix[3] = '\0';

    if(pos[0] <= monitors[0]){
        puts("Monitor 1");
        strcpy(suffix, ".a");
    }
    else if(pos[0] <= (monitors[0] + monitors[1])){
        puts("Monitor 2");
        strcpy(suffix, ".b");
    }else{
        puts("Monitor 3");
        strcpy(suffix, ".c");
    }

    int size = sizeof(value) + sizeof(suffix);
    char ws_name[size];
    strcpy(ws_name, value);
    strcat(ws_name, suffix);
    if(transport){
        printf("name: %s transport: %d\n", ws_name, 0);
        horiz(ws_name, 1);
    }
    else{
        printf("name: %s transport: %d\n", ws_name, 0);
        horiz(ws_name, 0);
    }

    return 0;
}
