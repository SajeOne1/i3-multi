CC = gcc
CFLAGS = -Wall -g -lm -lX11
LDFLAGS =
 
# List of sources:
SOURCES = main.c
OUT = ./i3multi
 
LDFLAGS += `pkg-config --libs --cflags i3ipc-glib-1.0`
 
all:
	$(CC) $(SOURCES) -o $(OUT) $(CFLAGS) $(LDFLAGS)
clean:
	rm $(OUT)
